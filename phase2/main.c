#include <stdio.h>
#include "main.h"




int main()
{
	//initialization
	setupRegs();
	
	open_led();
  open_serial();
	
	timer_ini();
	//open system
	//while(1);
	uint16_t result;
	char  data[10];

	sendString("Add 32bit int: ", 15);
	result = add(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Add 64bit int: ", 15);
	result = add(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
	
	sendString("Multiply 32bit int: ", 20);
	result = multiply(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Multiply 64bit int: ", 20);
	result = multiply(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
	
	sendString("Divide 32bit int: ", 18);
	result = divide(32);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Divide 64bit int: ", 18);
	result = divide(64);
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Copy 8 Byte structure: ", 23);
	result = structure_8();
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');

	sendString("Copy 128 Byte structure: ", 25);
	result = structure_128();
	sprintf(data,"%x",result);
	sendString(data,4);
	sendByte('\r');
	sendByte('\n');
	
	
	sendByte('\r');
	sendByte('\n');
	return 0;
}

void USART2_IRQHandler(void){
	uint8_t data;
	//receive command first
	data = getByte();
	//if not type in "enter", keep reading 
	if(data != 0x0D){
		//if not receiving backspace, record command, and send the byte that user types in
		if(data != 0x7F){
				inBuffer[inBufferCounter]=data;
				sendByte(data);
				inBufferCounter++;
			}
		//if receving backspace, record noting, and move pointer backward. 
			else{
				sendByte(data);
				if(inBufferCounter != 0){
					inBufferCounter--;
				}
			}
	}
	else{
		sendByte('\r');
		sendByte('\n');
		//when enter the "enter", run afterward function, and clear the pointer
		run();
		inBufferCounter = 0;
		sendByte('\r');
		sendByte('\n');
	}
}

void TIM2_IRQHandler(void){
	//sendByte('T');
	
	* regTIM2_SR &= ~0x1;
}
