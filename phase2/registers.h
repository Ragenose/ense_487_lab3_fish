/** registers.h 
 */
#include "stdint.h"

typedef uint32_t volatile * Reg32;

#define PERIPH_BASE       ((uint32_t)0x40000000)

#define APB1PERIPH_BASE   (PERIPH_BASE)
#define APB2PERIPH_BASE   (PERIPH_BASE + 0x10000)
#define AHBPERIPH_BASE    (PERIPH_BASE + 0x20000)

#define RCC_BASE          (AHBPERIPH_BASE + 0x1000)
#define RCC_APB1ENR			  (RCC_BASE + 0x1C)
#define RCC_APB2ENR       (RCC_BASE + 0x18)
#define RCC_CFGR					(RCC_BASE + 0x04)

#define GPIOB_BASE        (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR         (GPIOB_BASE + 0x0C)
#define GPIOB_CRH         (GPIOB_BASE + 0x04)
#define GPIOB_BSRR        (GPIOB_BASE  + 0x10)
#define GPIOB_BRR         (GPIOB_BASE  + 0x14)

#define GPIOA_BASE	  		(APB2PERIPH_BASE + 0x0800)
#define GPIOA_CRL	  			(GPIOA_BASE + 0x00)
#define GPIOA_BRR	  			(GPIOA_BASE + 0x14)
#define GPIOA_BSRR	  		(GPIOA_BASE + 0x10)

#define USART2_BASE	  		(APB1PERIPH_BASE + 0x4400)
#define USART2_BRR	  		(USART2_BASE + 0x08)
#define USART2_SR	  			(USART2_BASE + 0x00)
#define USART2_DR	  			(USART2_BASE + 0x04)
#define USART2_CR1	  		(USART2_BASE + 0x0C)
#define USART2_CR2	  		(USART2_BASE + 0x10)
#define USART2_CR3	  		(USART2_BASE + 0x14)

#define NVIC_BASE					(0xE000E100)
#define NVIC_ISER0				(NVIC_BASE)
#define NVIC_ISER1				(NVIC_BASE + 0x04)


#define TIM2_BASE					(PERIPH_BASE)	
#define TIM2_CR1					(TIM2_BASE)
#define TIM2_PSC					(TIM2_BASE + 0X28)
#define TIM2_ARR					(TIM2_BASE + 0X2C)
#define TIM2_CNT					(TIM2_BASE + 0X24)
#define TIM2_DIER					(TIM2_BASE + 0X0C)
#define TIM2_SR						(TIM2_BASE + 0x10)


#define BUFFER_SIZE 30
 
void setupRegs(void);
