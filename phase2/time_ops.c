#include "stdlib.h"
#include "timer_impl.h"



uint32_t random_32(void){
	uint32_t result = rand()%65535; //16
	result = (result << 16) + rand()%65535;
	return result;
}

uint64_t random_64(void){
	uint64_t result = random_32();
	result = (result << 32) + random_32();
	return result;
}

uint16_t add(int length){
	uint32_t rst;
	uint32_t num1,num2;
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int i = 0;i <100;i++){
		switch(length){
			case 32:
				num1 = random_32();
				num2 = random_32();
				startTime = timer_start();
				rst=num1+num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			case 64:
				num3 = random_64();
				num4 = random_64();
				startTime = timer_start();
				rst=num3+num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		rst=0;
	}
	return sum/100;
}

uint16_t multiply(int length){
	
	uint32_t num1,num2;
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int i = 0;i <100;i++){
		switch(length){
			case 32:
				num1 = random_32();
				num2 = random_32();
				startTime = timer_start();
				num1*num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			case 64:
				num3 = random_64();
				num4 = random_64();
				startTime = timer_start();
				num3*num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		
	}
	return sum/100;
}

uint16_t divide(int length){
	uint32_t num1,num2;
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int i = 0;i <100;i++){
		switch(length){
			case 32:
				num1 = random_32();
				num2 = random_32();
				while(num2 == 0){
					num2 = random_32();
				}
				startTime = timer_start();
				num1/num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			case 64:
				num3 = random_64();
				num4 = random_64();
				while(num4 == 0){
					num4 = random_64();
				}
				startTime = timer_start();
				num3/num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		
	}
	return sum/100;
}


uint16_t structure_8(){
	struct_8 test,temp;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int j = 0;j <100;j++){
			for(int i=0;i<2;i++){
				test.random[i] = random_32();
			}
			startTime = timer_start();
			temp = test;
			stopTime=timer_stop(startTime);
			sum+=stopTime;
	}
	return sum/100;
}

uint16_t structure_128(){
	struct_128 test,temp;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int j = 0;j <100;j++){
			for(int i=0;i<32;i++){
				test.random[i] = random_32();
			}
			startTime = timer_start();
			temp = test;
			stopTime=timer_stop(startTime);
			sum+=stopTime;
	}
	return sum/100;
}

uint16_t structure_1024(){
	struct_1024 test, temp;
	
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(int j = 0;j <100;j++){
			for(int i=0;i<128;i++){
				test.random[i] = random_64();
			}
			startTime = timer_start();
			temp = test;
			stopTime=timer_stop(startTime);
			sum+=stopTime;
			free(&test);
			free(&temp);
	}
	return sum/100;
}
