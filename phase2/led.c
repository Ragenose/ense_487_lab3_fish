#include "led.h"




void open_led(void){
	*regRCC_APB2ENR |= 0x08; // Enable Port B clock
	*regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
	*regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull 
}

void close_led(void){
	*regRCC_APB2ENR &= ~0x08;
	*regGPIOB_CRH  = 0x44444444;
}
/*
led parameter should be 0-7 for led 1-8
*/
void turn_on_led(uint8_t led){
	*regGPIOB_BSRR = MASK_BIT8 << led;
}

/*
led parameter should be 0-7 for led 1-8
*/
void turn_off_led(uint8_t led){
	*regGPIOB_BSRR = MASK_BIT24 << led;
}

uint8_t getLedStatus(uint8_t led){
	return (*regGPIOB_ODR & (MASK_BIT8 << led)) >> (led + 8);
}
