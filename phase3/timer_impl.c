#include "timer_impl.h"


void timer_ini(void){
	* regRCC_APB1ENR |= 0x1;
	* regTIM2_PSC     = 0xffff;
	*	regTIM2_ARR     = 1098;
	* regTIM2_CR1    |= 0x211;
	
	* regNVIC_ISER0  |= 0x10000000;
	* regTIM2_DIER	 |= 0x01;
}
	
	
uint16_t timer_start(void){
	return * regTIM2_CNT;
	
}

uint16_t timer_stop(uint16_t start_time){
	uint16_t stop_time = *regTIM2_CNT;
	return ((0xFFFF-start_time+stop_time))%0xFFFF;
}

void timer_shutdown(void){
	//disable TIM2
	* regTIM2_CR1 *= ~0x11;
	//Reset ARR register
	* regTIM2_ARR *= ~0xFFFF;
	//Disable TIM2 clock
	* regRCC_APB1ENR *= ~0x1;
}