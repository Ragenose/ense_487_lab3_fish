#include "registers.h"
#include "util.h"

extern Reg32 regRCC_APB2ENR;
extern Reg32 regGPIOB_ODR;
extern Reg32 regGPIOB_CRH;
extern Reg32 regGPIOB_BSRR;


void open_led(void);
void close_led(void);

void turn_on_led(uint8_t);
void turn_off_led(uint8_t);
uint8_t getLedStatus(uint8_t);
