#include "stdint.h"
#include "registers.h"

extern Reg32 regRCC_APB1ENR;
extern Reg32 regTIM2_CR1;
extern Reg32 regTIM2_ARR;
extern Reg32 regTIM2_PSC;
extern Reg32 regTIM2_SR;

void timer_ini(void);
