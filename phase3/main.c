#include "main.h"

int main(){
	setupRegs();
	open_led();
	timer_ini();
	//timer_start();
	while(1);
	
	return 0;
}

void TIM2_IRQHandler(void){
	
	//toggle led status
	if(getLedStatus(1)){
		turn_off_led(1);
	}
	else{
		turn_on_led(1);
	}
	//clear interrupt flag
	* regTIM2_SR &= ~0x1;
}
