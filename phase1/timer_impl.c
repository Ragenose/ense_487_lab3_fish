#include "timer_impl.h"


void timer_ini(void){
	* regRCC_APB1ENR |= 0x1;
	* regTIM2_PSC     = 0;
	*	regTIM2_ARR    |= 0xFFFF;
	* regTIM2_CR1    |= 0x11;
	
}
	
	
uint16_t timer_start(void){
	* regTIM2_CNT = 0xFFFF;
	* regTIM2_CR1 |= 0x1;
	
	return * regTIM2_CNT;
	
}

uint16_t timer_stop(uint16_t start_time){
	
	uint16_t stop_time = *regTIM2_CNT;
	
	return ((0xFFFF-start_time+stop_time))%0xFFFF;
}

void timer_shutdown(void){
	* regTIM2_CR1 *= ~0x11;
	* regTIM2_ARR *= ~0xFFFF;
	* regRCC_APB1ENR *= ~0x1;
}