#include "timer.h"

extern Reg32 regTIM2_CNT;

uint16_t timer_start(void);
uint16_t timer_stop(uint16_t);
void timer_shutdown(void);